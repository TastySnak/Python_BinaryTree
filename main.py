# Program to create a binary tree class that can be used in daily coding exercises.
# Python already has libraries which contain the binary tree data structure, but this is done for the sake of exercise.

import random


class BinaryTreeNode:
    def __init__(self, data=None):
        self.left = None
        self.right = None
        self.data = data

    def insert(self, data):
        if not self.data:
            self.data = data
            return

        # Randomly chooses a branch.
        # 0 for left, 1 for right
        branch = random.choice('01')

        if branch == '0':
            if self.left:
                self.left.insert(data)
                return
            self.left = BinaryTreeNode(data)
            return

        if self.right:
            self.right.insert(data)
            return
        self.right = BinaryTreeNode(data)

    # Returns a list of values collected using preorder traversal
    def preorder(self, values):
        if self.data is not None:
            values.append(self.data)
        if self.left is not None:
            self.left.preorder(values)
        if self.right is not None:
            self.right.preorder(values)
        return values

    # Returns a list of values collected using inorder traversal
    def inorder(self, values):
        if self.left is not None:
            self.left.inorder(values)
        if self.data is not None:
            values.append(self.data)
        if self.right is not None:
            self.right.inorder(values)
        return values

    # Returns a list of values collected using postorder traversal
    def postorder(self, values):
        if self.left is not None:
            self.left.postorder(values)
        if self.right is not None:
            self.right.postorder(values)
        if self.data is not None:
            values.append(self.data)
        return values


# Example
numbers = [5, 12, 9, 3, 16, 7, 1, 10]
binary_tree = BinaryTreeNode()
for num in numbers:
    binary_tree.insert(num)
print("Original List:")
print(numbers)
print("Preorder Traversal:")
print(binary_tree.preorder([]))
print("Inorder Traversal:")
print(binary_tree.inorder([]))
print("Postorder Traversal:")
print(binary_tree.postorder([]))

